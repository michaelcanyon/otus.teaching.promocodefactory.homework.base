﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public record EmployeeDto
    {
        public string FirstName { get; init; }

        public string LastName { get; init; }

        public string Email { get; init; }

        public List<Guid> RoleIds { get; init; }

        public int AppliedPromocodesCount { get; init; }
    }
}
