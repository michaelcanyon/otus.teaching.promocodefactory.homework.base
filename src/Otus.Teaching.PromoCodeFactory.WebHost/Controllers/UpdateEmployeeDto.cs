﻿using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    public record UpdateEmployeeDto: EmployeeDto
    {
        public Guid? Id { get; init; }
    }
}
