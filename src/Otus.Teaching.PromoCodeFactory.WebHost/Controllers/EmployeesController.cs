﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _roleRepository;

        public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> roleRepository)
        {
            _employeeRepository = employeeRepository;
            _roleRepository = roleRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Создать запись сотрудника
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost("Create")]
        public async Task<ActionResult<Guid>> Create(EmployeeDto data)
        {
            var roles = await _roleRepository.GetAllAsync();
            var config = new MapperConfiguration(cfg =>
            cfg
            .CreateMap<EmployeeDto, Employee>()
            .ForMember(
                x => x.Roles, 
                y => y.MapFrom(src => 
                    roles.Where(r => src.RoleIds.Contains(r.Id)))));
            var mapper = config.CreateMapper();

            return await _employeeRepository.Create(mapper.Map<Employee>(data));
        }

        /// <summary>
        /// Обновить запись сотрудника
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPut("Update")]
        public async Task<ActionResult<EmployeeResponse>> Update(UpdateEmployeeDto data)
        {
            var roles = await _roleRepository.GetAllAsync();
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<UpdateEmployeeDto, Employee>()
                   .ForMember(
                        x => x.Roles,
                        y => y.MapFrom(
                            src => roles.Where(r => src.RoleIds.Contains(r.Id))));
                cfg.CreateMap<Role, RoleItemResponse>();
                cfg.CreateMap<Employee, EmployeeResponse>();
            });
            var mapper = config.CreateMapper();

            var response = await _employeeRepository.Update(mapper.Map<Employee>(data));
            return mapper.Map<Employee, EmployeeResponse>(response);
        }

        /// <summary>
        /// Удалить запись сотрудника
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("Delete")]
        public async Task Delete(Guid id)
            => await _employeeRepository.Delete(id);
    }
}