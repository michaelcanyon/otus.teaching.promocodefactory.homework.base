﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<Guid> Create(T model)
        {
            model.Id = Guid.NewGuid();
            Data = Data.ToList().Concat(new[] { model });
            return Task.FromResult(model.Id);
        }
        public Task<T> Update(T model)
        {
            var item = Data.FirstOrDefault(i => i.Id == model.Id)
                ?? throw new Exception("Коллекция не содержит элемента с указанным Id");
            Data = Data.Where(i => i.Id != item.Id).Concat(new[] { model });
            return Task.FromResult(model);
        }

        public Task Delete(Guid id)
        {
            var item = Data.FirstOrDefault(i => i.Id == id)
                ?? throw new Exception("Коллекция не содержит элемента с указанным Id");
            Data = Data.Where(i => i.Id != id);
            return Task.CompletedTask;
        }
    }
}